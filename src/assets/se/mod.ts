import MineExploded from "./mine-exploded.mp3";
import MineFound from "./mine-found.mp3";
import Investigate from "./investigate.mp3";
import Safe from "./safe.mp3";
export { MineExploded, MineFound, Investigate, Safe };