export type Cell = {
    isRevealed: {
        isMine: boolean,
        nearbyMines: boolean
    },
    isMine: boolean,
    isExploded: boolean,
    nearbyMines: number
}

export interface boardConfig {
    size: [number, number],
    mines: number
}

export interface playerConfig {
    initial_location: [number, number],
    initial_cost: number,
    cost_weight: {
        investigate: number,
        dig: number,
        move: number,
        damaged: number
    },
    goal: number
}
export class Game {
    board: {
        cells: Cell[][],
    };
    player_state: {
        remaining_cost: number,
        remaining_til_goal: number
        location: [number, number],
    };
    config: { board: boardConfig, player: playerConfig }
    constructor(board_config: boardConfig, player_config: playerConfig) {
        this.config = { board: board_config, player: player_config };
        this.player_state = {
            remaining_cost: player_config.initial_cost,
            remaining_til_goal: player_config.goal,
            location: player_config.initial_location,
        }
        this.board = {
            cells: new Array(board_config.size[0]).fill(null)
                .map(() => new Array(board_config.size[1]).fill(null)
                    .map(() => ({ isRevealed: { isMine: false, nearbyMines: false }, isMine: false, isBlazed: false, nearbyMines: 0 })))
        };
        for (let i = 0; i < board_config.mines; i++) {
            let x = Math.floor(Math.random() * board_config.size[0]);
            let y = Math.floor(Math.random() * board_config.size[1]);
            if (this.board.cells[x][y].isMine || (x === player_config.initial_location[0] && y === player_config.initial_location[1])) {
                i--;
            } else {
                this.board.cells[x][y].isMine = true;
                for (let dx = -1; dx <= 1; dx++) {
                    for (let dy = -1; dy <= 1; dy++) {
                        if (x + dx >= 0 && x + dx < board_config.size[0] && y + dy >= 0 && y + dy < board_config.size[1]) {
                            this.board.cells[x + dx][y + dy].nearbyMines++;
                        }
                    }
                }
            }
        }
        this.board.cells[player_config.initial_location[0]][player_config.initial_location[1]].isRevealed.isMine = true;
    }
    isOver(): boolean {
        return this.player_state.remaining_til_goal <= 0 || this.player_state.remaining_cost <= 0
    }

    didPlayerWin(): boolean {
        return this.player_state.remaining_til_goal <= 0;
    }
    investigateCell(location: [number, number]): number {
        if (!this.board.cells[location[0]][location[1]].isRevealed.nearbyMines) {
            this.board.cells[location[0]][location[1]].isRevealed.nearbyMines = true;
            this.player_state.remaining_cost -= this.config.player.cost_weight.investigate;
        }
        return this.board.cells[location[0]][location[1]].nearbyMines;
    }
    digCell(location: [number, number]): boolean {
        if (!this.board.cells[location[0]][location[1]].isRevealed.isMine) {
            this.board.cells[location[0]][location[1]].isRevealed.isMine = true;
            this.player_state.remaining_cost -= this.config.player.cost_weight.dig;
            if (this.board.cells[location[0]][location[1]].isMine) {
                this.player_state.remaining_til_goal--;
            }
            return this.board.cells[location[0]][location[1]].isMine;
        }
        return false;
    }
    movePlayer(location: [number, number]): "accept" | "bomb" {
        this.player_state.remaining_cost -= this.config.player.cost_weight.move;
        this.player_state.location = location;
        if (this.board.cells[location[0]][location[1]].isMine && !this.board.cells[location[0]][location[1]].isRevealed.isMine) {
            this.board.cells[location[0]][location[1]].isRevealed.isMine = true;
            this.board.cells[location[0]][location[1]].isExploded = true;
            this.player_state.remaining_cost -= this.config.player.cost_weight.damaged;
            return "bomb";
        } else {
            this.board.cells[location[0]][location[1]].isRevealed.isMine = true;
            return "accept";
        }
    }
}